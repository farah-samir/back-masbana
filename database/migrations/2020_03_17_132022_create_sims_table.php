<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sims', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('email')->nullable();
            $table->string('region')->nullable();
            $table->string('irradtion')->nullable();
            $table->string('surface')->nullable();
            $table->string('occcupant')->nullable();
            $table->string('totalchauffagekwh')->nullable();
            $table->string('totalchauffageeuro')->nullable();
            $table->string('totalchaudkwh')->nullable();
            $table->string('totalchaudeuro')->nullable();
            $table->string('totalelectrokwh')->nullable();
            $table->string('totalegneralkwh')->nullable();
            $table->string('totalegneraleuro')->nullable();
            $table->string('totalcuissonkwh')->nullable();
            $table->string('totalcuisoneuro')->nullable();
            $table->string('puissance')->nullable();
            $table->string('pta')->nullable();
            $table->string('eaca')->nullable();
            $table->string('ra')->nullable();
            $table->string('se')->nullable();
            $table->string('ecd')->nullable();
            $table->string('ecv')->nullable();
            
            $table->string('totalgeneraleeuro')->nullable();

            $table->string('totalgeneralkwh')->nullable();
            $table->string('typechaud')->nullable();
            $table->string('typechauf')->nullable();

            $table->string('typecuiss')->nullable();


            $table->string('selectedchfg')->nullable();
            $table->string('chaude')->nullable();

            $table->string('cuisson')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sims');
    }
}
