<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sim extends Model
{
    
    protected $fillable = [

        'region',
        'irradtion',
        'surface',
        'occcupant',
        'totalchauffagekwh',
        'totalchauffageeuro',
        'totalchaudkwh',
        'totalchaudeuro',
        'totalelectrokwh',
        'totalcuissonkwh',
        'totalcuisoneuro',
        'puissance',
        'pta',
        'eaca',
        'ra',
        'se',
        'ecd',
        'ecv',
        'email',
        'totalgeneraleeuro',
         'totalgeneralkwh',
         'typecuiss',
         'typechaud',
         'typechauf',
         'selectedchfg',
         'chaude',
         'cuisson'

        
    ];

}
