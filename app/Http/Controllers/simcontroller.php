<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sim;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;


class simcontroller extends Controller
{
    public function index(){
    $sim = Sim::orderBy('created_at', 'desc')->get();

    echo json_encode($sim);
    
      }
 public function show($sim_id){
        return Sim::find($sim_id);
      }
    
    
    
 public function store(Request $request){
        $sim = new sim();
        

        $sim->region  =  $request->input('region');
        $sim->irradtion = $request->input('irradtion');
        $sim->surface =$request->input('surface');
        $sim->occcupant = $request->input('occcupant');
        $sim->totalchauffagekwh = $request->input('totalchauffagekwh');
        $sim->totalchauffageeuro = $request->input('totalchauffageeuro');
        $sim->totalchaudkwh = $request->input('totalchaudkwh');
        $sim->totalchaudeuro = $request->input('totalchaudeuro');
        $sim->totalelectrokwh = $request->input('totalelectrokwh');
        $sim->totalcuissonkwh = $request->input('totalcuissonkwh');
        $sim->totalcuisoneuro = $request->input('totalcuisoneuro');
        $sim->puissance = $request->input('puissance');
        $sim->pta = $request->input('pta');
        $sim->eaca = $request->input('eaca');
        $sim->ra = $request->input('ra');
        $sim->se = $request->input('se');
        $sim->ecd = $request->input('ecd');
        $sim->ecv  = $request->input('ecv');
        $sim->email  =  $request->input('email');
        $sim->totalgeneraleeuro  =  $request->input('totalgeneraleeuro');
        $sim->totalgeneralkwh  =  $request->input('totalgeneralkwh');
       
        $sim->typecuiss  =  $request->input('typecuiss');
        $sim->typechaud=  $request->input('typechaud');
        $sim->typechauf  =  $request->input('typechauf');
        $sim->selectedchfg  =  $request->input('selectedchfg');

        $sim->chaude  =  $request->input('chaude');

        $sim->cuisson  =  $request->input('cuisson');

      

        Mail::to('admin@gmail.com')->send(new SendEmail($sim));

          $sim->save();
        echo json_encode($sim);
    
    }


}
